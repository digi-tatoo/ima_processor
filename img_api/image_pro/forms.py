from django.contrib.auth.forms import \
    AuthenticationForm, \
    UserCreationForm, \
    UsernameField
from django.contrib.auth.models import User


class MyAuthticationForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        for n, f in self.fields.items():
            f.widget.attrs.update({"class": "form-control", 'placeholder': n})


class MyUserCreateForm(UserCreationForm):
    new_username = UsernameField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for n, f in self.fields.items():
            f.widget.attrs.update({"class": "form-control", "placeholder": n})

    class Meta:
        model = User
        fields = ()
        field_classes = {}


