from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth import login as auth_login
from django.contrib.auth.views import LoginView, LogoutView
from image_pro import forms as my_forms
from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin


class MyLoginView(SuccessMessageMixin, LoginView):
    template_name = 'login.html'
    form_class = my_forms.MyAuthticationForm
    success_message = "You were successfully logged in"
    extra_context = {'register_form': my_forms.MyUserCreateForm}

    def post(self, request, *args, **kwargs):
        action_type = request.POST['action_type']
        if action_type == 'create':
            form2 = my_forms.MyUserCreateForm(request.POST)
            if form2.is_valid():
                a = 1
        else:
            form1 = my_forms.MyAuthticationForm(request.POST)
            if form1.is_valid():
                auth_login(self.request, form1.get_user())
                return HttpResponseRedirect(self.get_success_url())





class MyLogoutView(LogoutView):
    template_name = 'login.html'
    extra_context = {'server_name': settings.SERVER_NAME}


def index(request):
    return render(request, 'main.html')
