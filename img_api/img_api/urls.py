"""img_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib import admin
from . import views
from image_pro import views as img_view

urlpatterns = [
    path('', views.slash),
    path('index/', img_view.index, name='index'),
    path('login/', img_view.MyLoginView.as_view(), name='login'),
    path('logout/', img_view.MyLogoutView.as_view(), name='logout'),
    path('admin/', admin.site.urls)
]


