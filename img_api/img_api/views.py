from django.shortcuts import render, HttpResponseRedirect
# Create your views here.


def slash(request):
    redirect_addr = r'index' if request.user.is_authenticated else r'/login'
    return HttpResponseRedirect(redirect_addr)


